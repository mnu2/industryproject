public class EasyAI extends Player {

    public EasyAI(){}

    public String getGuess(){

        String guess = CodeGenerator.generateCode();
        guesses.add(guess);
        return guess;
    }
}
