import java.util.List;

public class HardAI extends Player {

    public HardAI(){
        FiveGuessAlgorithm.populateS();
    }


    @Override
    public String getGuess(){return null;}
    public String getGuess(int turns) {
        //TODO use the FiveGuessAlgorithm to generate a new guess.
        if (turns == 1){
            guesses.add("9876");
            return "9876";
        }
        else {
            String guess = FiveGuessAlgorithm.generateGuess();
            guesses.add(guess);
            return guess;
        }
    }
}
