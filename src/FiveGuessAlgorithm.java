

import java.util.*;


public class FiveGuessAlgorithm {
    /*Take the set S of all possible guesses and pick a guess g.  Compare it to the code to get a score, s.  Eliminate from S all
    * codes.txt that achieve the score s when compared to g to give a new set S'.  Now consider S/{g}, taking an element e from
    * this set determine the number of elements it would eliminate from S' for each possible score and give it a grade equal to the
    * minimum number of elements it would eliminate from S'.  Doing this for all of S/{g} we can rank all the possible guesses and
    * we can choose our next guess by picking code with a maximal score.  We repeat this until we've cracked the code.*/

    /*Ranking: consider $g\in S/{g} for x\in {(0, 0), (0, 1), (0, 2), ..., (1, 0), (1, 1), ..., (4, 0)} assume
    * f(c, g) = x (where c is the code we're trying to determine) and determine all the y\in S' s.t. f(g, y) = x.
    * Rank g based off of the minimum number of elements of S' it would eliminate which is equal to |S'| - highest hit count.
    * Your next guess should be an element that maximizes this rank.*/

    /*To implement this strategy I'll need to create a list of all possible codes.txt and all possible comparison results.*/

    /*NOTE: even though I'm calling this the five guess algorithm it so far has solved everything in 5-6 guesses - I assume this
    * has to do with the slight rule differences between Knuth's defn of the game and the fact that we have 5040 potential codes
    * vs 1296 in Knuth's version*/

   public static List<String> S = new ArrayList<>();//will need to run CodeGenerator.generateSet() in BullsandCows to initialise S.

    private static int[][] Outcomes = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4},{1, 0}, {1, 1}, {1, 2}, {1, 3},
            {2, 0}, {2, 1}, {2, 2}, {3, 0}};//all the possible BullsandCows outcomes that don't end the game.


    private FiveGuessAlgorithm(){}

    public static String generateGuess(){
        int min = 5040;
        String guess = null;
        List<String> guesses = new ArrayList<>();

        for (String s: CodeGenerator.SET){
            int rank = rank(s);

            if(rank < min){
                min = rank;
                guess = s;
            }
        }

        for (String s: S){
            if (rank(s) == min){
                guesses.add(s);
            }
        }

        if (guesses.size()!= 0){//if the set of elements in S that are of minimal rank has size greater than 1 we want the smallest element,
                                //which is the first element as S is a sorted set.
            return guesses.get(0);
        }

        return guess;
    }


    //TODO this should implement a method that cycles through the set CodeGenerator.SET ranking each element based on the minimum
    //TODO number of elements each code would eliminate from S.  The input "code" should come from our potential guesses set S.
    private static int rank(String code){
        Map<int[], Integer> rankingOutcomes = new HashMap<int[], Integer>();

        for(int[] o: Outcomes) rankingOutcomes.put(o, 0);

        int rank = 0;
        for(String s: S){//fills the map rankingOutcomes
            int[] result = CodeGenerator.compareGuess(code, s);
            for(int[] r: rankingOutcomes.keySet()) {
                if (r[0] == result[0] && r[1] == result[1]) {
                    int count = rankingOutcomes.get(r);
                    rankingOutcomes.replace(r, ++count);
                }
            }
        }

        for(int[] r: rankingOutcomes.keySet()){
            if (rankingOutcomes.get(r) > rank ){
                rank = rankingOutcomes.get(r);
            }
        }
        return rank;
    }

    //TODO this should remove from S all the elements that don't give the result 'result' when compared to the
    //TODO computer's previous guess.  I construct an array list of results to be removed from S in order to avoid a
    //TODO ConcurrencyModificationException.
    public static void pruneS(int[] result, String guess){
        List<String> removalSet = new ArrayList<>();

        for (String s: S){
            int[] comp = CodeGenerator.compareGuess(s, guess);
            if((comp[0] != result[0]) || (comp[1]!= result[1])){
                removalSet.add(s);
            }
        }
            S.removeAll(removalSet);
    }

    public static void populateS(){//we want to copy over the values of SET without haveing S and Set point at the same object
        S.addAll(CodeGenerator.SET);
    }
}
