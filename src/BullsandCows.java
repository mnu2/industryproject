

import ictgradschool.Keyboard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


public class BullsandCows {
//in this approach there's no need for a human player to be an extension of the player class there just needs to be three
    //different computer players: easyAI, medAI, hardAI

    private String player_code = "";
    private String player_guess = "";
    private String aiLvl = "";
    private int turns = 0;
    private List<String> preSuppliedCodes = new ArrayList<>();
    private List<String> playerGuesses = new ArrayList<>();

    private void start() {
        readCodesFromFile();

        Player computer = chooseDifficulty();

        computer.setCode();

        setPlayerCodeOrGuess();


        //actual gameplay
        while (turns < 7) {
            System.out.println("***Round " + (turns + 1) + "***\n");
            setPlayerCodeOrGuess();
            String ai_Guess = computerGuess(computer);
            String bulls = "bulls";
            String cows = "cows";


            int[] result1 = CodeGenerator.compareGuess(player_guess, computer.getCode());//compares human guess to ai code
            int[] result2 = CodeGenerator.compareGuess(player_code, ai_Guess);//compares ai guess to human code
            CodeGenerator.RESULTS.add(result1);
            CodeGenerator.RESULTS.add(result2);
            if (result1[0] == 1){ bulls = "bull";}
            if(result1[1] == 1){cows = "cow";}

            System.out.println("Result: " + result1[0] + " " + bulls + " and " + result1[1] + " " + cows +"\n");

            if (result1[0] == 4 && result1[1] == 0) {//checks to see if the human won
                System.out.println("The human player wins");
                break;
            }

            if (result2[0] == 1){ bulls = "bull";}
            else{bulls = "bulls";}
            if(result2[1] == 1){cows = "cow";}
            else{cows = "cows";}

            System.out.println("Computer guess: " + ai_Guess);
            System.out.print("Result: " + result2[0] + " " + bulls + " and " + result2[1] + " " + cows +"\n");

            if (result2[0] == 4 && result2[1] == 0) {//checks to see if the computer won
                System.out.println("The computer wins");
                break;
            }
            System.out.println("---");

            if (computer instanceof HardAI) {
                FiveGuessAlgorithm.pruneS(result2, ai_Guess);
            }
            turns++;
        }

        if (turns == 7) {
            System.out.println("Neither player guessed the other's code: draw.");
        }
        saveToFile(computer);
    }

    //Allows the human player to choose the difficulty of the AI
    private Player chooseDifficulty() {
        boolean computerSet = false;
        Player computerChoice = null;
        while (!computerSet) {
            System.out.println("Please enter the difficulty level:");
            String choice = Keyboard.readInput();
            switch (choice.trim().toLowerCase()) {
                case "easy":
                    computerChoice = new EasyAI();
                    aiLvl = "easy";
                    computerSet = true;
                    break;
                case "medium":
                    computerChoice = new MedAI();
                    aiLvl = "medium";
                    computerSet = true;
                    break;
                case "hard":
                    computerChoice = new HardAI();
                    aiLvl = "hard";
                    computerSet = true;
                    break;
                default:
                    System.out.println("Your choice must be easy, medium or hard.");
            }
        }

        return computerChoice;
    }

    private void setPlayerCodeOrGuess() {//sets the player's code if unset otherwise sets player's guesses
        boolean goodCode = false;
        while (!goodCode) {
            if (player_code.equals("")) {
                System.out.print("Please enter your secret code: ");
                String code = Keyboard.readInput();
                if (!CodeGenerator.SET.contains(code)) {
                    System.out.println("Your code must be a valid 4-digit integer.  Try Again.");
                } else {
                    player_code = code;
                    System.out.println("---");
                    goodCode = true;
                }
            } else {
                String guess = "";
                System.out.print("Your guess: ");
                if (preSuppliedCodes.size() <= turns) {
                    guess = Keyboard.readInput();
                } else {
                    guess = preSuppliedCodes.get(turns);
                    System.out.println(guess);
                }
                if (!CodeGenerator.SET.contains(guess)) {
                    System.out.println("Your guess must be a valid 4-digit integer.  Try Again.");
                } else {
                    playerGuesses.add(guess);
                    player_guess = guess;
                    goodCode = true;
                }
            }
        }
    }

    private String computerGuess(Player player) {
        String computerGuess = "";
        switch (aiLvl) {
            case ("easy"):
                computerGuess = player.getGuess();
                break;
            case ("medium"):
                computerGuess = player.getGuess();
                break;
            case ("hard"):
                computerGuess = ((HardAI) player).getGuess(turns + 1);
                break;
        }
        return computerGuess;
    }

    private void readCodesFromFile() {
        System.out.println("Would you like to use presupplied codes?");
        boolean goodAnswer = false;
        while (!goodAnswer) {
            String response = Keyboard.readInput().toLowerCase().trim();

            if (response.trim().toLowerCase().equals("yes")) {
                boolean fileExists = false;
                while (!fileExists) {
                    System.out.println("Please enter the file name you would like to read from:");
                    String file = Keyboard.readInput();
                    File myFile = new File(file);
                    fileExists = myFile.exists();

                    try (Scanner scanner = new Scanner(myFile)) {
                        while (scanner.hasNextLine()) {
                            preSuppliedCodes.add(scanner.nextLine());
                        }
                        Iterator<String> myIterator = preSuppliedCodes.iterator();

                        while (myIterator.hasNext()) {
                            String elem = myIterator.next();
                            if (elem.isEmpty() || elem.charAt(0) == ' ') {
                               myIterator.remove();
                            }
                        }
                        goodAnswer = true;
                    } catch (IOException e) {
                        System.out.println("Please enter a valid filename");
                    }
                }

            } else if (response.trim().toLowerCase().equals("no")) {
                goodAnswer = true;
            } else {
                System.out.println("Please enter 'Yes' or 'No' and then a valid file name");
            }
        }
    }

    private void saveToFile(Player player){
        System.out.println("Would you like to save the results to a text file?");
        String response = Keyboard.readInput();
        if(response.trim().toLowerCase().equals("yes")){
            System.out.println("Please pick a file you would like to save to: ");
            String file = Keyboard.readInput();
            File myFile = new File(file);
            try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile))){

                bufferedWriter.write("Bulls and Cows game result\n");
                bufferedWriter.write("Your code: " + player_code + "\n");
                bufferedWriter.write("The computers code: " + player.getCode() + "\n");

                for(int i = 0; i < playerGuesses.size(); i++){
                    String bulls = "bulls";
                    String cows = "cows";
                    bufferedWriter.write("***Round " + (i + 1) + "***" + "\n");

                    if (CodeGenerator.RESULTS.get(2*i)[0] == 1){ bulls = "bull";}
                    if(CodeGenerator.RESULTS.get(2*i)[1] == 1){cows = "cow";}

                    bufferedWriter.write("You guessed " + playerGuesses.get(i) + ", this scored " + CodeGenerator.RESULTS.get(2*i)[0] + " " + bulls + " and "
                            + CodeGenerator.RESULTS.get(2*i)[1] + " " + cows + "\n");
                    if (CodeGenerator.RESULTS.get(2*i)[0] == 4){
                        bufferedWriter.write("You win!!!");
                        bufferedWriter.write("---");
                        break;
                    }
                    if (CodeGenerator.RESULTS.get(2*i + 1)[0]==1) { bulls = "bull"; }
                    else{bulls = "bulls";}
                    if(CodeGenerator.RESULTS.get(2*i + 1)[1]==1){ cows = "cow"; }
                    else{cows = "cows";}

                    bufferedWriter.write("The computer guessed " + player.guesses.get(i) + ", this scored " + CodeGenerator.RESULTS.get(2*i + 1)[0] + " " + bulls +" and "
                            + CodeGenerator.RESULTS.get(2*i + 1)[1] + " " + cows + " " + "\n");
                    if (CodeGenerator.RESULTS.get(2*i + 1)[0] == 4){
                        bufferedWriter.write("You lose :(" + "\n");
                        break;
                    }
                    bufferedWriter.write("---" + "\n");
                    if (i == 6){bufferedWriter.write("Draw!!!");}
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        else {System.out.println("Okay!");}
    }


    public static void main(String[] args) {
        BullsandCows ex = new BullsandCows();
        ex.start();
    }
}

