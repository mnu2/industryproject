import ictgradschool.Keyboard;

import java.util.ArrayList;
import java.util.List;

public abstract class Player {

    public Player(){
        CodeGenerator.generateSet();
    }

    protected String code;
    protected List<String> guesses = new ArrayList<>();

    //Will depend on whether the player is human or computer.
    public void setCode(){
            this.code = CodeGenerator.generateCode();
    }

    public String getCode(){
        return this.code;
    }

    public List<String> getGuesses() {
        return guesses;
    }

    //Again, will depend on whether the player is human or computer, and then
    //the level of AI.
    public abstract String getGuess();

}
