import java.util.ArrayList;
import java.util.List;


public class MedAI extends Player {

    public MedAI(){}

    public String getGuess() {
        String guess = null;
        boolean go = true;
        while(go) {
            guess = CodeGenerator.generateCode();
            if (!guesses.contains(guess)) {
                guesses.add(guess);
                go = false;
            }
        }
        return guess;
    }

}
