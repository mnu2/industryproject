import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CodeGenerator {

    private CodeGenerator(){}

    public static List<String> SET = new ArrayList<>();
    public static List<int[]> RESULTS = new ArrayList<>();


    //Generates an ArrayList of numbers 1-9 and randomly gets and removes a number from the list
    private static List<Integer> generateList() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        return list;
    }
    //Generates a valid 4-digit code.
    public static String generateCode() {
        List<Integer> list = generateList();
        String code="";
        for (int i = 0; i < 4; i++) {
            int k = (int) (list.size() * Math.random());
            code += list.get(k);
            list.remove(k);
        }
        return code;
    }

    //Generates the set of all possible codes.txt - necessary for the FiveGuessAlgorithm.
    public static void generateSet(){
        int i = 0;
        while (SET.size()< 5040){
            String option = generateCode();
            if (!SET.contains(option)){
                SET.add(option);
            }
        }
        Collections.sort(SET);
    }

    //Compares two codes.txt and returns an array that states the number of bulls and cows
    public static int[] compareGuess(String hum_code, String ai_code){
        int[] bullsncows = {0, 0};//the first input is the number of bulls, the second the number of cows
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if ((hum_code.charAt(i) == ai_code.charAt(j)) && i == j) {
                    bullsncows[0]++;
                }
                else if (hum_code.charAt(i) == (ai_code.charAt(j))){
                    bullsncows[1]++;
                }
            }
        }
        return bullsncows;
    }
}
